#[macro_use]
extern crate bitflags;

use nebbot_utils::*;

use serenity:: {
    framework::standard::{
        StandardFramework,
    },
};

use serenity::prelude::*;

use std::env;
use std::sync::Arc;

mod cache;
use cache::*;

mod db;
use db::Db;

mod osu;
use osu::Osu;

mod commands;
use commands::*;

mod events;
use events::*;

#[tokio::main]
async fn main() {
    dotenv::dotenv().unwrap();

    let osu = Mutex::new(Osu::new(env::var("OSU_TOKEN").expect("Token required in environmental variable OSU_TOKEN")));

    let db = Mutex::new(Db::new().await.expect("Could not start database"));

    let token = env::var("DISCORD_TOKEN").expect("Token required in environmental variable DISCORD_TOKEN");

    let mut client = Client::new(&token)
        .event_handler(Handler)
        .framework(StandardFramework::new()
        .configure(|c| c.prefix("o!"))
        .help(&HELP)
        .group(&GENERAL_GROUP)
        .group(&OSUCOMMANDS_GROUP)
        .after(commands::after)
        ).await
        .expect("Error creating client");

    info!("Created client!");
        
    info!("Listening for events!");

    {
        let mut data = client.data.write().await;

        data.insert::<OsuContainer>(Arc::new(osu));
        data.insert::<ShardManagerContainer>(client.shard_manager.clone());
        data.insert::<DbContainer>(Arc::new(db));
    }

    if let Err(why) = client.start().await {
        error!("{:?}", why);
    }
}