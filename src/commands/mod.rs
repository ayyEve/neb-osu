mod general;
pub use general::*;

mod osu;
pub use osu::*;

mod config;
pub use config::*;

mod help;
pub use help::*;

use serenity::framework::standard::{ CommandResult, macros::hook };
use serenity::prelude::*;
use serenity::model::prelude::*;

use nebbot_utils::*;

#[hook]
pub async fn after(_ctx: &Context, _msg: &Message, command_name: &str, command_result: CommandResult) {
    match command_result {
        Ok(_) => (),
        Err(e) => error!("Command {} -- {:?}", command_name, e),
    }
}