use serenity::framework::standard::{
    CommandResult,
    Args,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use nebbot_utils::*;

#[command]
#[aliases("b", "top", "t")]
async fn best(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let (account, mode) = match super::parse_args(args, &*osu.lock().await, &*database.lock().await, *msg.author.id.as_u64()).await {
        Ok(a) => match a { 
            Some((a,m)) => (a,m),
            None => {
                msg.channel_id.say(&ctx.http, "Provide a valid user argument").await?;
                return Ok(());
            }
        }
        Err(e) => {
            error!("Command best parse_args - {}", e);
            return Err(e);
        }
    };

    let profile = match osu.lock().await.get_user(account.clone(), mode).await {
        Ok(p) => p,
        Err(e) => {
            error!("Command best get_user - {}", e);
            return Err(Box::new(e));
        }
    };

    let _scores = match osu.lock().await.get_best(account.clone(), mode, 5).await {
        Ok(b) => b,
        Err(e) => {
            error!("Command best get_best - {}", e);
            return Err(Box::new(e));
        }
    };
    let score = match _scores.first() {
        Some(s) => s,
        None => {
            msg.channel_id.say(&ctx.http, "No best score found.").await?;
            return Ok(());
        }
    };

    msg.channel_id.send_message(&ctx.http, |m| score.embed(m, profile)).await?;

    Ok(())
}