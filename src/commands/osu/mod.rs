use serenity::framework::standard::{
    CommandError,
    Args,
    macros::group
};

use crate::osu::account::OsuAccount;
use crate::osu::mode::OsuMode;
use crate::osu::Osu;
use crate::db::Db;

mod user;
pub use user::*;

mod best;
pub use best::*;

mod latest;
pub use latest::*;

mod sync;
pub use sync::*;

mod unsync;
pub use unsync::*;

mod set_mode;
pub use set_mode::*;

#[group("Osu")]
#[commands(user, best, latest, sync, unsync, set_mode)]
struct OsuCommands;

async fn parse_args(mut args: Args, osu: &Osu, db: &Db, id: u64) -> Result<Option<(OsuAccount, OsuMode)>, CommandError> {

    let mut account: OsuAccount;
    let mode: OsuMode;

    match db.get_user(id).await? {
        Some(user) => {

            account = OsuAccount::from(user.osu_username.as_str());
    
            mode = match args.current() {
                Some(a) => match OsuMode::parse(a) {
                    Some(m) => m,
                    None => { 
                        let arg = match args.current() {
                            Some(a) => a,
                            None => { return Ok(None); }
                        };
                
                        account = match OsuAccount::from_dc(db, arg).await? {
                            Some(a) => a,
                            None =>  { return Ok(None); }
                        };
                
                        // mode
                        match args.advance().current() {
                            Some(a) => match OsuMode::parse(a) {
                                Some(m) => m,
                                None => OsuMode::Standard
                            },
                            None => {
                                match db.get_user_by_osu(osu, account.clone()).await {
                                    Ok(u) => match u {
                                        Some(i) => i.default_mode,
                                        None => OsuMode::Standard,
                                    }
                                    Err(e) => { return Err(Box::new(e)); },
                                }
                            }
                        }
                    },
                },
                None => user.default_mode,
            };
        }
        None => {
            {
                let arg = match args.current() {
                    Some(a) => a,
                    None => { return Ok(None); }
                };
        
                account = match OsuAccount::from_dc(db, arg).await? {
                    Some(a) => a,
                    None =>  { return Ok(None); }
                };
        
                mode = match args.advance().current() {
                    Some(a) => match OsuMode::parse(a) {
                        Some(m) => m,
                        None => OsuMode::Standard
                    },
                    None => {
                        match db.get_user_by_osu(osu, account.clone()).await {
                            Ok(u) => match u {
                                Some(i) => i.default_mode,
                                None => OsuMode::Standard,
                            }
                            Err(e) => { return Err(Box::new(e)); },
                        }
                    }
                };
            }
        }
    }

    Ok(Some((account, mode)))
}