use serenity::framework::standard::{
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use nebbot_utils::*;

#[command]
async fn unsync(ctx: &Context, msg: &Message) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    match database.lock().await.get_user(*msg.author.id.as_u64()).await {
        Ok(b) => if let None = b { 
            msg.channel_id.say(&ctx.http, "You are not already synced discord with osu!.").await?;
            return Ok(()); 
        },
        Err(e) => { 
            error!("Command sync has_user - {}", e);
            return Ok(());
        },
    }

    match database.lock().await.remove_user(*msg.author.id.as_u64()).await {
        Ok(_) => { 
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title(format!("Unsynced {}", msg.author.tag()))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            
            )).await?;
        },
        Err(e) => { 
            error!("Command sync has_user - {}", e);
            return Ok(());
        },
    }

    Ok(())
}