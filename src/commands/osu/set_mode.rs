use serenity::{

    framework::standard::{
        Args,
        CommandResult,
        macros::command
    }
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::osu::profile::{ PROFILE_PIC_URL, PROFILE_URL };
use crate::db::{ DbUserUpdater };
use crate::osu::mode::OsuMode;

use nebbot_utils::*;

#[command("mode")]
pub async fn set_mode(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let mode = match args.current() {
        Some(a) => match OsuMode::parse(a) {
            Some(m) => m,
            None => OsuMode::Standard
        },
        None => OsuMode::Standard
    };

    match database.lock().await.get_user(*msg.author.id.as_u64()).await {
        Ok(b) => if let None = b { 
            msg.channel_id.say(&ctx.http, "You need to first sync discord with osu!.").await?;
            return Ok(()); 
        },
        Err(e) => {
            error!("Database has_user error: {}", e);
            return Ok(());
        },
    }

    let user = match database.lock().await.get_user(*msg.author.id.as_u64()).await {
        Ok(u) => u.unwrap(), // Should have user, checked above
        Err(e) => {
            error!("Database has_user error: {}", e);
            return Ok(());
        }
    };

    match database.lock().await.update_user(DbUserUpdater::new(*msg.author.id.as_u64()).default_mode(mode)).await {
        Ok(_) => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                
                .title(format!("Updated default mode to {}", mode.name()))
                .colour(Colour::from_rgb(228, 132, 250))

                .author(|a| a
                    .name(user.osu_username)
                    .url(format!("{}/{}/{}", PROFILE_URL, user.osu_id, mode.url()))
                    .icon_url(format!("{}/{}", PROFILE_PIC_URL, user.osu_id))
                )

                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            
            )).await?;
        }
        Err(e) => {
            msg.channel_id.say(&ctx.http, e).await?;
        }
    }

    Ok(())
}