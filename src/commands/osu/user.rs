use serenity::framework::standard::{
    CommandResult,
    Args,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use nebbot_utils::*;

#[command]
#[aliases("u","profile","p","stats","s","info","i")]
async fn user(ctx: &Context, msg: &Message, args: Args) -> CommandResult {
    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let (account, mode) = match super::parse_args(args, &*osu.lock().await, &*database.lock().await, *msg.author.id.as_u64()).await {
        Ok(a) => match a { 
            Some((a,m)) => (a,m),
                None => {
                msg.channel_id.say(&ctx.http, "Provide a valid user argument").await?;
            return Ok(());
            }
        }
        Err(e) => {
            error!("Command user parse_args - {}", e);
            return Err(e);
        }
    };

    let profile = match osu.lock().await.get_user(account.clone(), mode).await {
        Ok(p) => p,
        Err(e) => {
            error!("Command user get_user - {}", e);
            return Err(Box::new(e));
        }
    };

    let best = match osu.lock().await.get_best(account.clone(), mode, 5).await {
        Ok(b) => b,
        Err(e) => {
            error!("Command user get_best - {}", e);
            return Err(Box::new(e));
        }
    };

    //msg.delete(&ctx.http).await?;
    msg.channel_id.send_message(&ctx.http, |m| profile.embed_with_best(m, mode, &best)).await?;

    Ok(())
}