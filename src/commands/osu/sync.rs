use serenity::framework::standard::{
    Args,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use serenity::utils::Colour;

use crate::osu::profile::{ PROFILE_PIC_URL, PROFILE_URL };
use crate::osu::account::OsuAccount;
use crate::osu::mode::OsuMode;

use crate::db::DbUser;

use nebbot_utils::*;

#[command]
async fn sync(ctx: &Context, msg: &Message, mut args: Args) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

    let arg = match args.current() {
        Some(a) => a,
        None => { 
            msg.channel_id.say(&ctx.http, "Provide a valid user argument").await?;
            return Ok(());
        }
    };

    let account = OsuAccount::from(arg);

    let mode = match args.advance().current() {
        Some(a) => match OsuMode::parse(a) {
            Some(m) => m,
            None => OsuMode::Standard
        },
        None => OsuMode::Standard
    };

    match database.lock().await.get_user(*msg.author.id.as_u64()).await {
        Ok(b) => if let Some(_) = b { 
            msg.channel_id.say(&ctx.http, "Already synced discord with osu!.").await?;
            return Ok(()); 
        },
        Err(e) => { 
            error!("Command sync has_user - {}", e);
            return Ok(());
        },
    }

    let profile = match osu.lock().await.get_user(account.clone(), mode).await {
        Ok(p) => p,
        Err(e) => {
            error!("Command sync get_user - {}", e);
            return Ok(());
        }
    };

    let user = DbUser::new(*msg.author.id.as_u64(), profile.user_id,
        profile.username.clone(), mode.clone());

    match database.lock().await.add_user(user.clone()).await {
        Ok(_) => {
            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| e
                .title(format!("Synced {} with user {}", msg.author.tag(), user.osu_username))

                .thumbnail(format!("{}/{}", PROFILE_PIC_URL, user.osu_id))
                .colour(Colour::from_rgb(228, 132, 250))
                .url(format!("{}/{}/{}", PROFILE_URL, user.osu_id, mode.url()))
                .footer(|f| f
                    .icon_url(msg.author.avatar_url().unwrap_or(msg.author.default_avatar_url()))
                    .text(msg.author.tag())
                )
            
            )).await?;
        }
        Err(e) => {
            error!("Command sync add_user - {}", e);
            return Ok(());
        }
    }

    Ok(())
}