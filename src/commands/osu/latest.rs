use serenity::framework::standard::{
    CommandResult,
    Args,
    macros::command
};

use serenity::prelude::*;
use serenity::model::prelude::*;

use nebbot_utils::*;

#[command]
#[aliases("recent","r","l","newest","new","n")]
async fn latest(ctx: &Context, msg: &Message, args: Args) -> CommandResult {

    let data = ctx.data.read().await;
    let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
    let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");
    
    let (account, mode) = match super::parse_args(args, &*osu.lock().await, &*database.lock().await, *msg.author.id.as_u64()).await {
        Ok(a) => match a { 
            Some((a,m)) => (a,m),
            None => {
                msg.channel_id.say(&ctx.http, "Provide a valid user argument").await?;
                return Ok(());
            }
        }
        Err(e) => {
            error!("Command latest parse_args - {}", e);
            return Err(e);
        }
    };

    let profile = match osu.lock().await.get_user(account.clone(), mode).await {
        Ok(p) => p,
        Err(e) => {
            error!("Command latest get_user - {}", e);
            return Err(Box::new(e));
        }
    };

    let _scores = match osu.lock().await.get_recent(account, mode, 1).await{
        Ok(r) => r,
        Err(e) => {
            error!("Command latest get_recent - {}", e);
            return Err(Box::new(e));
        }
    };


    let score = match _scores.first() {
        Some(s) => s,
        None => {
            msg.channel_id.say(&ctx.http, "No recent score found.").await?;
            return Ok(());
        }
    };

    msg.channel_id.send_message(&ctx.http, |m| score.embed(m, profile)).await?;

    Ok(())
}