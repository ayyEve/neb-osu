use serenity::{

    framework::standard::{
        Args,
        CommandResult,
        macros::{
            command,
            group,
        }
    },
};

use serenity::prelude::*;
use serenity::model::prelude::*;

#[group]
#[prefix("config")]
#[only_in(guilds)]
#[required_permissions("MANAGE_CHANNELS")]
#[commands(set_auto_channel)]
struct Config;

#[command("auto")]
pub async fn set_auto_channel(_ctx: &Context, _msg: &Message, _args: Args) -> CommandResult {
    

    Ok(())
}
