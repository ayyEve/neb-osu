#![allow(non_upper_case_globals)]

bitflags! {
    pub struct OsuMods: u32 {
       const NoFail         = 1;
       const Easy           = 2;
       const TouchDevice    = 4;
       const Hidden         = 8;
       const HardRock       = 16;
       const SuddenDeath    = 32;
       const DoubleTime     = 64;
       const Relax          = 128;
       const HalfTime       = 256;
       const Nightcore      = 512; // Only set along with DoubleTime. i.e: NC only gives 576
       const Flashlight     = 1024;
       const Autoplay       = 2048;
       const SpunOut        = 4096;
       const Relax2         = 8192;    // Autopilot
       const Perfect        = 16384; // Only set along with SuddenDeath. i.e: PF only gives 16416  
       const Key4           = 32768;
       const Key5           = 65536;
       const Key6           = 131072;
       const Key7           = 262144;
       const Key8           = 524288;
       const FadeIn         = 1048576;
       const Random         = 2097152;
       const Cinema         = 4194304;
       const Target         = 8388608;
       const Key9           = 16777216;
       const KeyCoop        = 33554432;
       const Key1           = 67108864;
       const Key3           = 134217728;
       const Key2           = 268435456;
       const ScoreV2        = 536870912;
       const Mirror         = 1073741824;
       const KeyMod = Self::Key1.bits | Self::Key2.bits | Self::Key3.bits | Self::Key4.bits | Self::Key5.bits | Self::Key6.bits | Self::Key7.bits | Self::Key8.bits | Self::Key9.bits | Self::KeyCoop.bits;
       const FreeModAllowed = Self::NoFail.bits | Self::Easy.bits | Self::Hidden.bits | Self::HardRock.bits | Self::SuddenDeath.bits | Self::Flashlight.bits | Self::FadeIn.bits | Self::Relax.bits | Self::Relax2.bits | Self::SpunOut.bits | Self::KeyMod.bits;
       const ScoreIncreaseMods = Self::Hidden.bits | Self::HardRock.bits | Self::DoubleTime.bits | Self::Flashlight.bits | Self::FadeIn.bits;
    }
}

impl OsuMods {
    pub fn short_list(&self) -> String {

        // EZ NF HT
        // HR SD/PF DT HD FL

        if self.is_empty() {
            return String::new();
        }

        let mut mods = String::from("+");

        if self.contains(OsuMods::Easy) {
            mods += "EZ,"
        }

        else if self.contains(OsuMods::HardRock) {
            mods += "HR,"
        }

        if self.contains(OsuMods::NoFail) {
            mods += "NF,"
        }

        else if self.contains(OsuMods::Perfect) {
            mods += "PF,"
        }

        else if self.contains(OsuMods::SuddenDeath) {
            mods += "SD,"
        }

        if self.contains(OsuMods::HalfTime) {
            mods += "HT,"
        }

        else if self.contains(OsuMods::Nightcore) {
            mods += "NC,"
        }

        else if self.contains(OsuMods::DoubleTime) {
            mods += "DT,"
        }

        if self.contains(OsuMods::Hidden) {
            mods += "HD,"
        }

        if self.contains(OsuMods::Flashlight) {
            mods += "FL,"
        }

        if self.contains(OsuMods::SpunOut) {
            mods += "SO,"
        }

        if mods.chars().last().unwrap() == ',' {
            mods.remove(mods.len() - 1);
        }

        mods
    }

    pub fn difficulty_increasing(&self) -> Self {
        let mut temp = OsuMods::empty();

        if self.contains(OsuMods::HardRock) {
            temp.insert(OsuMods::HardRock);
        }

        if self.contains(OsuMods::DoubleTime) {
            temp.insert(OsuMods::DoubleTime);
        }

        if self.contains(OsuMods::Easy) {
            temp.insert(OsuMods::Easy);
        }

        if self.contains(OsuMods::HalfTime) {
            temp.insert(OsuMods::HalfTime);
        }

        temp
    }
}