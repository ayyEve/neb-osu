use nebbot_utils::*;

#[derive(Copy, Clone, Debug)]
pub enum OsuMode{
    Standard,
    Mania,
    Taiko,
    CatchTheBeat
}

impl OsuMode {
    pub fn parse(s: &str) -> Option<OsuMode> {
        match s.replace("!", "").to_ascii_lowercase().as_str() {
            "s" | "osu" | "os" | "stnd" | "ostd" | "ost" | "osustandard" |
            "std" | "ostandard" | "o" | "circle" | "circles" |
            "standard" => Some(OsuMode::Standard),
            
            "m" | "osumania" | "osum" | "omania" | "k" |
            "om" | "man" | "osuman" | "keys" | "key" | "piano" |
            "mania" => Some(OsuMode::Mania),

            "t" | "tai" | "tko" | "otk" |
            "tk" | "otaiko" | "osutaiko" | "drum" | "drums" |
            "taiko" => Some(OsuMode::Taiko),

            "c" | "ctbt" | "fruit" | "ocatch" | "octb" |
            "catch" | "catb" | "osuctb" | "osucatch" | "osucatchthebeat" |
            "ctb" => Some(OsuMode::CatchTheBeat),

            _ => None
        }
    }

    pub fn int(&self) -> u8 {
        match self {
            OsuMode::Standard => 0,
            OsuMode::Taiko => 1,
            OsuMode::CatchTheBeat => 2,
            OsuMode::Mania => 3,
        }
    }

    pub fn from_int(i: u8) -> OsuMode {
        match i {
            0 => OsuMode::Standard,
            1 => OsuMode::Taiko,
            2 => OsuMode::CatchTheBeat,
            3 => OsuMode::Mania,
            
            _ => OsuMode::Standard
        }
    }

    pub fn name(&self) -> &str {
        match self {
            OsuMode::Standard => "osu!",
            OsuMode::Taiko => "osu!taiko",
            OsuMode::CatchTheBeat => "osu!catch",
            OsuMode::Mania => "osu!mania",
        }
    }

    pub fn url(&self) -> &str {
        match self {
            OsuMode::Standard => "",
            OsuMode::Taiko => "taiko",
            OsuMode::CatchTheBeat => "fruits",
            OsuMode::Mania => "mania",
        }
    }
}

impl DbType for OsuMode {
    type Compatible = i16;

    fn convert(&self) -> Self::Compatible {
        self.int() as i16
    }

    fn from_row(row: &tokio_postgres::Row, index: usize) -> Self {
        DbType::from_sql(row.get::<usize, i16>(index))
    }

    fn from_sql(sql: Self::Compatible) -> Self {
        OsuMode::from_int(sql as u8)
    }
}