use crate::osu::{
    scores::OsuScore,
    mods::OsuMods,
};

pub fn calculate_pp(score: &OsuScore) -> u32 {

    let object_count = score.beatmap.count_normal + score.beatmap.count_slider;

    let mut multiplier = 0.8;

    if score.mods.contains(OsuMods::Easy) { multiplier *= 0.5; }
    if score.mods.contains(OsuMods::SpunOut) { multiplier *= 0.95; }
    if score.mods.contains(OsuMods::NoFail) { multiplier *= 0.9; }

    let perfect_window = 64.0 - 3.0 * score.beatmap.od;

    let mut score_rate = 1.0;

    if score.mods.contains(OsuMods::Easy) { score_rate *= 0.5; }
    if score.mods.contains(OsuMods::HalfTime) { score_rate *= 0.5; }
    if score.mods.contains(OsuMods::NoFail) { score_rate *= 0.5; }

    let total_score = score.score as f32 / score_rate;

    if score.mods.contains(OsuMods::KeyMod) {
        return 0; // TEMP
    }

    let mut base_strain = (5.0 * 1f32.max(score.beatmap.star_rating / 0.2) - 4.0).powf(2.2) /
        135.0;
        
    base_strain *= 1.0 + 0.1 * 1f32.min(object_count as f32 / 1500.0);

    base_strain  *= if total_score < 500_000.0 { total_score / 500_000.0 * 0.10 } else
        if total_score < 600_000.0 { (total_score - 500_000.0) / 100_000.0 * 0.3 } else
        if total_score < 700_000.0 { (total_score - 600_000.0) / 100_000.0 * 0.25 + 0.3 } else
        if total_score < 800_000.0 { (total_score - 700_000.0) / 100_000.0 * 0.2 + 0.55 } else
        if total_score < 900_000.0 { (total_score - 800_000.0) / 100_000.0 * 0.15 + 0.75 } else
        { (total_score - 900_000.0) / 100_000.0 * 0.1 + 0.9 };

    let window_factor = 0.0f32.max(0.2 - ((perfect_window - 34.0) * 0.006667));

    let score_factor = (0.0f32.max(total_score - 960_000.0) / 40_000.0).powf(1.1);

    let base_acc = window_factor * base_strain * score_factor;

    let acc_factor = base_acc.powf(1.1);

    let strain_factor = base_strain.powf(1.1);    

    let raw = (acc_factor + strain_factor).powf(1.0/1.1) * multiplier;

    raw.round() as u32
}   