use crate::osu::{
    scores::OsuScore,
    mods::OsuMods,
};

const MIN_OD: f32 = 50.0;
const MAX_OD: f32 = 20.0;

pub fn calculate_pp(score: &OsuScore) -> u32 {

    let total_hits = score.beatmap.max_combo;

    let mut strain = (1.0f32.max(score.beatmap.star_rating / 0.0075) * 5.0 - 4.0).powi(2) / 100_000.0;
    
    let length_bonus = 1.0f32.min(total_hits as f32 / 1500.0) * 0.1 + 1.0;

    strain *= length_bonus;

    // ayyEve commented this line so ¯\_(ツ)_/¯
    strain *= 0.985f32.powi(score.count_miss as i32);

    //strain *= ((total_hits as f32).powf(0.5) / (score.max_combo as f32).powf(0.5)).min(1.0);

    strain *= score.accuracy;

    let mut multiplier = 1.1;

    if score.mods.contains(OsuMods::Hidden) { multiplier *= 1.1; strain *= 1.025; }
    if score.mods.contains(OsuMods::NoFail) { multiplier *= 0.9; }
    if score.mods.contains(OsuMods::Flashlight) { strain *= 1.05 * length_bonus; }

    let mut od = score.beatmap.od;

    if score.mods.contains(OsuMods::Easy) { od *= 0.5; }
    if score.mods.contains(OsuMods::HardRock) { od *= 1.4; }

    od = od.min(10.0).max(0.0);

    let mut result = MIN_OD + (MAX_OD - MIN_OD) * od * 0.1;

    result = result.floor() - 0.5;

    if score.mods.contains(OsuMods::HalfTime) { result /= 0.75; }
    else if score.mods.contains(OsuMods::DoubleTime) { result /= 1.5; }

    let hit_window = (result * 100.0).round() / 100.0;

    let acc = (150.0 / hit_window).powf(1.1) * score.accuracy.powi(15) * 22.0 
        * (total_hits as f32 / 1500.0).powf(0.3).min(1.15);

    let raw = (strain.powf(1.1) + acc.powf(1.1)).powf(1.0/1.1) * multiplier;

    raw.round() as u32
}   
