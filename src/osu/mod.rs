use reqwest::Url;
use reqwest::Result;
use reqwest::Client;

pub mod pp;

pub mod maps;
use maps::OsuBeatmap;

pub mod mods;
use mods::OsuMods;

pub mod grade;

pub mod scores;
use scores::{ OsuScore };

pub mod profile;
use profile::OsuProfile;

const URL: &str = "https://osu.ppy.sh/api";

pub mod account;
use account::OsuAccount;

pub mod mode;
use mode::OsuMode;

pub mod presence;
use presence::OsuPresence;

#[derive(Clone)]
pub struct Osu {
    token: String,
    client: Client,

    cache: Vec<OsuPresence>,
}

impl Osu {
    pub fn new(token: String) -> Osu {
        Osu { token, client: Client::new(), cache: Vec::new() }
    }

    pub async fn get_user(&self, account: OsuAccount, mode: OsuMode) -> Result<OsuProfile> {
        let link = format!("{}/get_user?k={}&u={}&m={}", URL, self.token, account.any(), mode.int());

        let url = Url::parse(link.as_str()).expect("Error parsing url");

        let response = self.client.get(url).send().await?.text().await?;

        let profile = OsuProfile::serialise(response).expect("Failed to serialise OsuProfile.");

        Ok(profile)
    }

    pub async fn get_recent(&self, account: OsuAccount, mode: OsuMode, amount: u32) -> Result<Vec<OsuScore>> {
        let link = format!("{}/get_user_recent?k={}&u={}&m={}&limit={}", URL, self.token, account.any(), mode.int(), amount);

        let url = Url::parse(link.as_str()).expect("Error parsing url");

        let response = self.client.get(url).send().await?.text().await?;

        let scores = OsuScore::serialise(response, &self, mode).await.expect("Failed to serialise OsuScore.");

        Ok(scores)
    }

    /*pub async fn get_pb_score(&self, account: OsuAccount, beatmap_id: u32, mode: OsuMode) -> Result<Option<OsuScore>> {
        let link = format!("{}/get_scores?k={}&b={}&u={}&m={}&limit={}", URL, self.token, beatmap_id, account.any(), mode.int(), 1);

        let url = Url::parse(link.as_str()).expect("Error parsing url");

        let response = self.client.get(url).send().await?.text().await?;

        let _scores = OsuScore::serialise(response, &self, mode).await.expect("Failed to serialise OsuScore.");

        let score = match _scores.first(){
            Some(s) => Some(s.clone()),
            None => None,
        };

        Ok(score)
    }*/

    pub async fn get_best(&self, account: OsuAccount, mode: OsuMode, amount: u32) -> Result<Vec<OsuScore>> {
        let link = format!("{}/get_user_best?k={}&u={}&m={}&limit={}", URL, self.token, account.any(), mode.int(), amount);

        let url = Url::parse(link.as_str()).expect("Error parsing url");

        let response = self.client.get(url).send().await?.text().await?;

        let best = OsuScore::serialise(response, &self, mode).await.expect("Failed to serialise OsuScore.");

        Ok(best)
    }

    /*pub async fn get_map(&self, id: u32, mods: OsuMods) -> Result<OsuBeatmap> {
        let link = format!("{}/get_beatmaps?k={}&b={}&limit=1&mods={}", URL, self.token, id, mods.difficulty_increasing().bits());

        let url = Url::parse(link.as_str()).expect("Error parsing url");

        let response = self.client.get(url).send().await?.text().await?;

        let best = OsuBeatmap::serialise(response, false).expect("Failed to serialise OsuBeatmap.");

        Ok(best)
    }*/

    pub async fn get_map_convert(&self, id: u32, mods: OsuMods, mode: OsuMode) -> Result<OsuBeatmap> {
        let link = format!("{}/get_beatmaps?k={}&b={}&limit=1&mods={}&m={}", URL, self.token, id, mods.difficulty_increasing().bits(), mode.int());

        let url = Url::parse(link.as_str()).expect("Error parsing url");

        let response = self.client.get(url).send().await?.text().await?;

        if response.trim() != "[]" {
            let best = OsuBeatmap::serialise(response, false).expect("Failed to serialise OsuBeatmap.");

            return Ok(best);
        }

        let convert_link = format!("{}/get_beatmaps?k={}&b={}&limit=1&mods={}&m={}&a=1", URL, self.token, id, mods.difficulty_increasing().bits(), mode.int());

        let convert_url = Url::parse(convert_link.as_str()).expect("Error parsing url");

        let convert_response = self.client.get(convert_url).send().await?.text().await?;

        let convert_best = OsuBeatmap::serialise(convert_response, true).expect("Failed to serialise OsuBeatmap.");

        Ok(convert_best)
    }

    pub async fn cache_presence(&mut self, presence: OsuPresence) {
        for (i, c) in self.cache.iter().enumerate() {
            if c.account.compare(&self, presence.account.clone()).await { 
                self.cache.remove(i);

                break;
            }
       }

        self.cache.push(presence);
    }

    pub async fn get_cached_presence(&self, account: OsuAccount) -> Option<OsuPresence> {
        for c in self.cache.iter() {
            if c.account.compare(&self, account.clone()).await {
                return Some(c.clone());
            }
       }

       None
    }
}