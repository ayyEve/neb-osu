use serde_json::Value;
use serde_json::Result;

use serenity::builder::{ CreateMessage };
use serenity::utils::Colour;

use num_format::{ Locale, ToFormattedString };

use sha2::{ Sha256, Digest };
use byteorder::{ BigEndian, WriteBytesExt };

use nebbot_utils::*;

use crate::osu::{ 
    profile::{ PROFILE_URL, PROFILE_PIC_URL, OsuProfile },
    mode::OsuMode,
    presence::OsuPresence, 
    maps::OsuBeatmap, 
    mods::OsuMods,
    grade::OsuGrade,
    pp::{ standard, mania, taiko },
    Osu,
};

const BEATMAPSET_THUMBNAIL_START: &str = "https://b.ppy.sh/thumb/";
const BEATMAPSET_THUMBNAIL_END: &str = "l.jpg";

const BEATMAP_URL: &str = "https://osu.ppy.sh/beatmapsets";

use chrono::prelude::*;

#[derive(Clone, Debug)]
pub struct OsuScore {
    pub beatmap_id: u32,
    pub beatmap: OsuBeatmap,
    pub mode: OsuMode,
    pub score_id: u32,
    pub user_id: u32,
    pub score: u32,
    pub accuracy: f32,
    pub max_combo: u32,
    pub count_300: u32,
    pub count_300p: u32, //geki
    pub count_100: u32,
    pub count_100p: u32, //katu
    pub count_50: u32,
    pub count_miss: u32,
    pub fc: bool,
    pub mods: OsuMods,
    pub rank: OsuGrade,
    pub pp: u32,
    pub date: DateTime<Utc>,
}

impl OsuScore {
    pub async fn serialise(json: String, osu: &Osu, mode: OsuMode) -> Result<Vec<OsuScore>> {
        let values: Value = serde_json::from_str(json.as_str())?;
        
        let mut vec: Vec<OsuScore> = Vec::new();

        if let Value::Array(a) = values {
            for val in a {
                let mut beatmap_id: u32 = 0;
                let mut score_id: u32 = 0;
                let mut user_id: u32 = 0;
                let mut score: u32 = 0;
                let mut max_combo: u32 = 0;
                let mut count_300: u32 = 0;
                let mut count_300p: u32 = 0; //geki
                let mut count_100: u32 = 0;
                let mut count_100p: u32 = 0; //katu
                let mut count_50: u32 = 0;
                let mut count_miss: u32 = 0;
                let mut fc: bool = false;
                let mut mods: OsuMods = OsuMods::empty();
                let mut rank: OsuGrade = OsuGrade::Fail;
                let mut pp: u32 = 0;
                let mut date: DateTime<Utc> = chrono::MIN_DATETIME;

                if let Value::String(s) = &val["beatmap_id"] {
                    beatmap_id = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["enabled_mods"] {
                    mods = OsuMods::from_bits(s.parse::<u32>().unwrap()).unwrap();
                }

                let beatmap: OsuBeatmap = osu.get_map_convert(beatmap_id, mods, mode).await.expect("Failed to get beatmap");

                if let Value::String(s) = &val["score_id"] {
                    score_id = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["user_id"] {
                    user_id = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["score"] {
                    score = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["maxcombo"] {
                    max_combo = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["count300"] {
                    count_300 = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["countgeki"] {
                    count_300p = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["count100"] {
                    count_100 = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["countkatu"] {
                    count_100p = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["count50"] {
                    count_50 = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["countmiss"] {
                    count_miss = s.parse::<u32>().unwrap();
                }

                if let Value::String(s) = &val["perfect"] {
                    fc = if s.parse::<u8>().unwrap() == 1 { true } else { false };
                }

                if let Value::String(s) = &val["rank"] {
                    rank = OsuGrade::from(&s.clone()).unwrap();
                }

                if let Value::String(s) = &val["pp"] {
                    pp = s.parse::<f32>().unwrap().ceil() as u32;
                }

                if let Value::String(s) = &val["date"] {
                    match Utc.datetime_from_str(s, "%Y-%m-%d %H:%M:%S") {
                        Ok(d) => date = d,
                        Err(e) => error!("{}", e),
                    }
                }

                let mut temp = OsuScore { beatmap_id, beatmap, mode, score_id, user_id, score, accuracy: 0.0, max_combo, count_300,
                    count_300p, count_100, count_100p, count_50, count_miss, fc,
                    mods, rank, pp, date };

                temp.calculate_accuracy();

                if pp == 0 {
                    temp.calculate_pp();
                }

                vec.push(temp);
            }
        }

        Ok(vec)
    }

    pub fn compare(&self, presence: OsuPresence) -> bool {

        let small_text = match presence.song {
            Some(s) => s,
            None => return false,
        };

        let score_text = format!("{} - {} [{}]", self.beatmap.artist, self.beatmap.title, self.beatmap.diff_name);

        let equal = score_text == small_text;

        let after = self.date > presence.time;

        //debug!("{} == {} = {}; {} > {} = {}", score_text, small_text, equal, self.date, presence.time, after);

        return equal && after;
    }

    pub fn embed<'a,'b>(&self, m: &'a mut CreateMessage<'b>, user: OsuProfile) -> &'a mut CreateMessage<'b> {

        let max_combo_text = if self.fc { " [FC]".to_owned() } else {
             if self.beatmap.max_combo == 0 { "".to_owned() } else { format!("/{}", self.beatmap.max_combo) } 
            };

        m.embed(|e| {
            e
            .thumbnail(format!("{}{}{}", BEATMAPSET_THUMBNAIL_START, self.beatmap.beatmapset_id, BEATMAPSET_THUMBNAIL_END))
            .colour(Colour::from_rgb(228, 132, 250))
            .title(format!("[{}] ({}) {} - {} [{}]", self.mode.name(), self.beatmap.creator.clone(), self.beatmap.artist.clone(),
             self.beatmap.title.clone(), self.beatmap.diff_name.clone()))
    
            .description(format!("{}```Score: {}    {}*    {:2.2}%    {}``` ```CS:{}    AR:{}    OD:{}    HP:{}    BPM:{}```",
                if self.beatmap.convert { "[Convert]\n" } else { "" },
                self.score.to_formatted_string(&Locale::en_GB), self.beatmap.star_rating,
                self.accuracy * 100.0, self.mods.short_list(), self.beatmap.cs, self.beatmap.ar,
                self.beatmap.od, self.beatmap.hp, self.beatmap.bpm))

            .author(|a| a
                .name(format!("{}", user.username))
                .url(format!("{}/{}/{}", PROFILE_URL, self.user_id, self.mode.url()))
                .icon_url(format!("{}/{}", PROFILE_PIC_URL, self.user_id))
            )

            .field("Rank", self.rank.clone().name(), true)
            .field("PP", self.pp, true)
            .field("Max Combo", format!("{}{}", self.max_combo, max_combo_text), true);

            match self.mode {
                OsuMode::Standard => { e
                    .field("300", self.count_300, true)
                    .field("300+", self.count_300p, true)
                    .field("100", self.count_100, true)
                    .field("100+", self.count_100p, true)
                    .field("50", self.count_50, true)
                    .field("Miss", self.count_miss, true)
                }

                OsuMode::Mania => { e
                    .field("300+", self.count_300p, true)
                    .field("300", self.count_300, true)
                    .field("200", self.count_100p, true)
                    .field("100", self.count_100, true)
                    .field("50", self.count_50, true)
                    .field("Miss", self.count_miss, true)
                }

                OsuMode::Taiko => { e
                    .field("Great", self.count_300, true)
                    .field("Good", self.count_100, true)
                    .field("Miss", self.count_miss, true)
                }

                OsuMode::CatchTheBeat => { e
                    .field("Fruits", self.count_300, true)
                    .field("Ticks", self.count_100, true)
                    .field("Droplets", self.count_50, true)
                    .field("Droplets Miss", self.count_100p, true)
                    .field("Miss", self.count_miss, true)
                }
            }
            
    
            .url(format!("{}/{}#{}/{}", BEATMAP_URL, self.beatmap.beatmapset_id, self.mode.url(), self.beatmap_id))
            
            .timestamp(self.date.to_rfc3339())
        })
    }

    fn calculate_accuracy(&mut self) {
        self.accuracy = match self.mode {
            OsuMode::Standard => {
                ( 6 * self.count_300
                + 2 * self.count_100
                + self.count_50 ) as f32 / (6 * (self.count_300 + self.count_100 + self.count_50 + self.count_miss)) as f32
            }

            OsuMode::Mania => {
                ( 6 * (self.count_300 + self.count_300p)
                + 4 * self.count_100p 
                + 2 * self.count_100
                + self.count_50 ) as f32 / (6 * (self.count_300 + self.count_300p + self.count_100 + self.count_100p + self.count_50 + self.count_miss)) as f32
            }

            OsuMode::Taiko => {
                ( 2 * self.count_300
                + self.count_100) as f32 / (2 * (self.count_300 + self.count_100 + self.count_miss)) as f32
            }

            OsuMode::CatchTheBeat => {
                ( self.count_300 + self.count_100 + self.count_50 ) as f32 
                / ( self.count_300 + self.count_100 + self.count_50 + self.count_100p + self.count_miss ) as f32
            }
        };
    }

    pub fn hash(&self) -> String{
        let sha = Sha256::new();

        let mut buffer = Vec::new();

        buffer.write_u32::<BigEndian>(self.beatmap_id).unwrap();
        buffer.write_u8(self.mode.int()).unwrap();
        buffer.write_u32::<BigEndian>(self.user_id).unwrap();
        buffer.write_u32::<BigEndian>(self.score).unwrap();
        buffer.write_u32::<BigEndian>(self.max_combo).unwrap();
        buffer.write_u32::<BigEndian>(self.count_300).unwrap();
        buffer.write_u32::<BigEndian>(self.count_300p).unwrap();
        buffer.write_u32::<BigEndian>(self.count_100).unwrap();
        buffer.write_u32::<BigEndian>(self.count_100p).unwrap();
        buffer.write_u32::<BigEndian>(self.count_50).unwrap();
        buffer.write_u32::<BigEndian>(self.count_miss).unwrap();
        buffer.write_u32::<BigEndian>(self.mods.bits()).unwrap();
        buffer.write_i64::<BigEndian>(self.date.timestamp()).unwrap();

        hex::encode(sha.chain(buffer).finalize())
    }

    fn calculate_pp(&mut self) {
        self.pp = match self.mode {
            OsuMode::Standard => standard::calculate_pp(&self),
            OsuMode::Mania => mania::calculate_pp(&self),
            OsuMode::Taiko => taiko::calculate_pp(&self),
            _ => 0,
        };
    }
}
