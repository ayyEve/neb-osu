use crate::osu::Osu;
use crate::osu::mode::OsuMode;

use crate::db::Db;

use regex::Regex;

#[derive(Clone, Debug)]
pub enum OsuAccount {
    Id(u32),
    Username(String),
}

impl OsuAccount {

    pub fn from(id_or_username: &str) -> OsuAccount {
        match id_or_username.parse::<u32>() {
            Ok(i) => OsuAccount::Id(i),
            Err(_) => {
                OsuAccount::Username(String::from(id_or_username))
            }
        }
    }

    pub async fn from_dc(db: &Db, dc_or_id_or_username: &str) -> Result<Option<OsuAccount>, tokio_postgres::Error> {
        let regex = Regex::new(r"^<@!*((\d)*)>$").unwrap();
        if regex.is_match(dc_or_id_or_username) {

            let capture = &regex.captures_iter(dc_or_id_or_username).next().unwrap()[1];

            let discord_id = capture.parse::<u64>().unwrap();

            match db.get_user(discord_id).await?
            {
                Some(user) => { return Ok(Some(OsuAccount::Id(user.osu_id))); },
                None => { return Ok(None); }
            }
        }

        Ok(Some(OsuAccount::from(dc_or_id_or_username)))
    }

    pub async fn find_id(&self, osu: &Osu) -> Option<u32> {
        if let OsuAccount::Id(id) = self {
            return Some(*id);
        }

        match osu.get_user(self.clone(), OsuMode::Standard).await {
            Ok(o) => Some(o.user_id),
            Err(_) => None,
        }
    }

    pub fn id(&self) -> Option<u32> {
        if let OsuAccount::Id(id) = self {
            return Some(*id);
        }
        None
    }

    pub fn is_id(&self) -> bool {
        if let OsuAccount::Id(_) = self {
            return true;
        }
        return false;
    }

    pub fn username(&self) -> Option<String> {
        if let OsuAccount::Username(u) = self {
            return Some(u.clone());
        }
        None
    }

    pub fn is_username(&self) -> bool {
        if let OsuAccount::Username(_) = self {
            return true;
        }
        return false;
    }

    pub fn any(&self) -> String {
        match self {
            OsuAccount::Id(id) => format!("{}", id),
            OsuAccount::Username(un) => un.clone(),
        }
    }

    pub async fn compare(&self, osu: &Osu, account: OsuAccount) -> bool {
        if self.is_id() && account.is_id() {
            return self.id() == account.id();
        }

        if self.is_username() && account.is_username() {
            return self.username() == account.username();
        }

        let s = match osu.get_user(self.clone(), OsuMode::Standard).await {
            Ok(o) => o.user_id,
            Err(_) => return false,
        };

        let a = match osu.get_user(account, OsuMode::Standard).await {
            Ok(o) => o.user_id,
            Err(_) => return false,
        };

        return s == a;
    }
}