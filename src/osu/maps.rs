use serde_json::Value;
use serde_json::Result;

#[derive(Clone, Debug)]
pub struct OsuBeatmap {
    pub beatmap_id: u32,
    pub beatmapset_id: u32,
    pub artist: String,
    pub title: String,
    pub creator: String,
    pub creator_id: u32,
    pub diff_name: String,
    pub bpm: f32,
    pub star_rating: f32,
    pub cs: f32,
    pub od: f32,
    pub ar: f32,
    pub hp: f32,

    pub aim: f32,
    pub speed: f32,

    pub max_combo: u32,
    pub count_normal: u32,
    pub count_slider: u32,
    pub count_spinner: u32,

    pub convert: bool,
}

impl OsuBeatmap {
    pub fn serialise(json: String, convert: bool) -> Result<OsuBeatmap> {
        let mut values: Value = serde_json::from_str(json.as_str())?;
        values = values[0].clone();

        let mut beatmap_id: u32 = 0;
        let mut beatmapset_id: u32 = 0;
        let mut artist: String = String::new();
        let mut title: String = String::new();
        let mut creator: String = String::new();
        let mut creator_id: u32 = 0;
        let mut diff_name: String = String::new();
        let mut bpm: f32 = 0.0;
        let mut star_rating: f32 = 0.0;
        let mut cs: f32 = 0.0;
        let mut od: f32 = 0.0;
        let mut ar: f32 = 0.0;
        let mut hp: f32 = 0.0;

        let mut aim: f32 = 0.0;
        let mut speed: f32 = 0.0;

        let mut max_combo: u32 = 0;
        let mut count_normal: u32 = 0;
        let mut count_slider: u32 = 0;
        let mut count_spinner: u32 = 0;

        if let Value::String(s) = &values["beatmap_id"] {
            beatmap_id = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["beatmapset_id"] {
            beatmapset_id = s.parse::<u32>().unwrap();
        }
        
        if let Value::String(s) = &values["artist"] {
            artist = s.clone();
        }

        if let Value::String(s) = &values["title"] {
            title = s.clone();
        }
        
        if let Value::String(s) = &values["creator"] {
            creator = s.clone();
        }
        
        if let Value::String(s) = &values["creator_id"] {
            creator_id = s.parse::<u32>().unwrap();
        }
        
        if let Value::String(s) = &values["version"] {
            diff_name = s.clone();
        }
        
        if let Value::String(s) = &values["bpm"] {
            bpm = (s.parse::<f32>().unwrap() * 10.0).round() / 10.0;
        }
        
        if let Value::String(s) = &values["difficultyrating"] {
            star_rating = (s.parse::<f32>().unwrap() * 100.0).round() / 100.0;
        }
        
        if let Value::String(s) = &values["diff_size"] {
            cs = (s.parse::<f32>().unwrap() * 10.0).round() / 10.0;
        }
        
        if let Value::String(s) = &values["diff_overall"] {
            od = (s.parse::<f32>().unwrap() * 10.0).round() / 10.0;
        }
        
        if let Value::String(s) = &values["diff_approach"] {
            ar = (s.parse::<f32>().unwrap() * 10.0).round() / 10.0;
        }
        
        if let Value::String(s) = &values["diff_drain"] {
            hp = (s.parse::<f32>().unwrap() * 10.0).round() / 10.0;
        }

        if let Value::String(s) = &values["diff_aim"] {
            aim = s.parse::<f32>().unwrap();
        }

        if let Value::String(s) = &values["diff_speed"] {
            speed = s.parse::<f32>().unwrap();
        }

        if let Value::String(s) = &values["max_combo"] {
            max_combo = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["count_normal"] {
            count_normal = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["count_slider"] {
            count_slider = s.parse::<u32>().unwrap();
        }

        if let Value::String(s) = &values["count_spinner"] {
            count_spinner = s.parse::<u32>().unwrap();
        }
        
        Ok(OsuBeatmap { beatmap_id, beatmapset_id, artist, title, creator, creator_id, diff_name, bpm, star_rating, cs, od, ar, hp, aim, speed,
            max_combo, count_normal, count_slider, count_spinner, convert })
    }
}