pub const APPLICATION_ID: u64 = 367827983903490050;

use crate::osu::mode::OsuMode;
use crate::osu::account::OsuAccount;

use chrono::prelude::*;

#[derive(Clone, Debug)]
pub struct OsuPresence {
    pub account: OsuAccount,
    pub state: String,
    pub mode: OsuMode,
    pub song: Option<String>,
    pub time: DateTime<Utc>,
}

