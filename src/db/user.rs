#![allow(dead_code)]

use nebbot_utils::*;
use crate::osu::mode::OsuMode;

db_table! {
    User {
        discord_id: u64,
        osu_id: u32,
        osu_username: String,
        default_mode: OsuMode,
        cached_recent_s: String,
        cached_recent_t: String,
        cached_recent_c: String,
        cached_recent_m: String,
        auto_guild_id: u64,
    }
}

impl DbUser {
    pub fn new(discord_id: u64, osu_id: u32, osu_username: String, default_mode: OsuMode) -> Self {
        DbUser {
            discord_id, osu_id, osu_username, default_mode, cached_recent_s: String::new(),
            cached_recent_t: String::new(), cached_recent_c: String::new(), cached_recent_m: String::new(),
            auto_guild_id: 0,
        }
    }
}