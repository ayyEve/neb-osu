#![allow(dead_code)]

use tokio::sync::Mutex;

mod user;
pub use user::{ DbUserUpdater, DbUser };

mod guilds;
pub use guilds::{ DbGuildUpdater, DbGuild };

use tokio_postgres::{ Client, Error, NoTls, types::ToSql };

use crate::osu::{ Osu, account::OsuAccount, mode::OsuMode };
use nebbot_utils::*;

pub struct Db {
    client: Mutex<Client>,
}

impl Db {
    pub async fn new() -> Result<Db, Error> {
        let config = std::env::var("DATABASE_CONFIG").expect("Config required in environmental variable DATABASE_CONFIG");
        let (client, conn) = tokio_postgres::connect(&config, NoTls).await?;

        tokio::spawn(async move {
            if let Err(e) = conn.await {
                error!("Database Connection Error -- {}", e);
            }
        });

        Ok(Db { client: Mutex::new(client) })
    }

    async fn select_user<T : ToSql + Sync>(&self, identifier: &'static str, id: T) -> Result<Option<DbUser>, Error> {
        let rows = self.client.lock().await.query(format!("SELECT * FROM \"Users\" WHERE {}=$1;", identifier).as_str(), &[&id]).await?;
        
        match rows.first() {
            Some(row) => Ok(Some(DbUser {
            discord_id: u64::from_row(&row, 0),
            osu_id: u32::from_row(&row, 1),
            osu_username: String::from_row(&row, 2),
            default_mode: OsuMode::from_row(&row, 3),
            cached_recent_s: String::from_row(&row, 4),
            cached_recent_t: String::from_row(&row, 5),
            cached_recent_c: String::from_row(&row, 6),
            cached_recent_m: String::from_row(&row, 7),
            auto_guild_id: u64::from_row(&row, 8),
        })),

            None => Ok(None)
        }
    }

    pub async fn get_user_by_osu(&self, osu: &Osu, account: OsuAccount) -> Result<Option<DbUser>, Error> {
        let osu_id = match account.find_id(osu).await {
            Some(o) => o,
            None => { return Ok(None); },
        };

        let user = self.select_user("osu_id", osu_id.to_string()).await?;

        Ok(user)
    }
}
