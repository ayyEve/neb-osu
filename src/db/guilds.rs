#![allow(dead_code)]

use nebbot_utils::*;

db_table! {
    Guild {
        guild_id: u64,
        auto_channel_id: u64,
    }
}

impl DbGuild {
    pub fn new(guild_id: u64) -> Self {
        DbGuild {
            guild_id, auto_channel_id: 0,
        }
    }
}

