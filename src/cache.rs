use serenity::client::bridge::gateway::ShardManager;

use serenity::prelude::*;

use std::sync::Arc;

use crate::osu::Osu;
use crate::db::Db;

pub struct OsuContainer;

impl TypeMapKey for OsuContainer {
    type Value = Arc<Mutex<Osu>>;
}

pub struct DbContainer;

impl TypeMapKey for DbContainer {
    type Value = Arc<Mutex<Db>>;
}

pub struct ShardManagerContainer;

impl TypeMapKey for ShardManagerContainer {
    type Value = Arc<Mutex<ShardManager>>;
}