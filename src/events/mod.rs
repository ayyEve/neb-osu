use nebbot_utils::*;

use serenity:: {
    async_trait,

    client::bridge::gateway::event::ShardStageUpdateEvent,
    gateway::ConnectionStage,

    model::{ 
        channel::{ Channel, GuildChannel },
        gateway::{ Ready, Activity },
        event::{ PresenceUpdateEvent, ResumedEvent },
    },
};

use serenity::prelude::*;
use chrono::prelude::*;

use serenity::utils::Colour;

use std::sync::Arc;

use crate::osu::{ grade::OsuGrade, account::OsuAccount, mode::OsuMode, presence::OsuPresence, Osu };
use crate::osu::profile::{ PROFILE_PIC_URL, PROFILE_URL };
use crate::db::{ Db, DbUser, DbUserUpdater };

pub struct Handler;

#[async_trait]
impl EventHandler for Handler {
    async fn ready(&self, _: Context, ready: Ready) {
        info!("{} is connected!", ready.user.name);
    }

    async fn resume(&self, _: Context, resume: ResumedEvent) {
        info!("Resumed! Trace: {:?}",  resume.trace);
    }

    async fn shard_stage_update(&self, _ctx: Context, update: ShardStageUpdateEvent) {
        
        info!("Shard {} update event: Old: {:?}; New: {:?}", update.shard_id, update.old, update.new);

        if let ConnectionStage::Connecting = update.new {
            warn!("Shard {} has disconnected! Reconnecting...", update.shard_id);
        }

        if let ConnectionStage::Resuming = update.new {
            warn!("Resuming Shard {}...", update.shard_id);
        }
    }

    async fn presence_update(&self, ctx: Context, update: PresenceUpdateEvent) {
        let time = Utc::now();

        let mut osu_activity: Option<Activity> = None; 
        for a in update.presence.activities {
            if let Some(id) = a.application_id {
                if *id.as_u64() == crate::osu::presence::APPLICATION_ID {
                    osu_activity = Some(a);
                    break;
                }
            }
        }

        if let Some(a) = osu_activity {

            let assets = match a.clone().assets {
                Some(a) => a,
                None => {
                    /*let _users = ctx.cache.users().await;

                    let dc_user = _users.get(&update.presence.user_id).unwrap();

                    dc_user.direct_message(&ctx.http, |m| m.embed(|e| e
                        .title("Error with osu! rich presence. Restart the game?")
                        .colour(Colour::from_rgb(228, 132, 250))
                        .footer(|f| f
                            .icon_url(dc_user.avatar_url().unwrap_or(dc_user.default_avatar_url()))
                            .text(dc_user.tag())
                        )
                    
                    )).await.unwrap();*/

                    return;
                },
            };

            let large_text = assets.large_text.unwrap();
            let username = if let Some(un) = large_text.split_whitespace().next() { un } else { " " };

            let account = OsuAccount::from(username);
            let mode = match OsuMode::parse(assets.small_text.unwrap().as_str()) {
                Some(m) => m,
                None => OsuMode::Standard,
            };

            let data = ctx.data.read().await;
            let osu = data.get::<crate::OsuContainer>().expect("Couldn't retrieve OsuContainer");
            let database = data.get::<crate::DbContainer>().expect("Couldn't retrieve DbContainer");

            if let Ok(o) = database.lock().await.get_user(*update.presence.user_id.as_u64()).await {
                if let None = o {
                    let _users = ctx.cache.users().await;
                    let dc_user = _users.get(&update.presence.user_id).unwrap();
    
                    let profile = osu.lock().await.get_user(account.clone(), mode).await.unwrap();
    
                    let user = DbUser::new(*dc_user.id.as_u64(), profile.user_id,
                        profile.username.clone(), mode.clone());
    
                    match database.lock().await.add_user(user.clone()).await {
                        Ok(_) => {
                            dc_user.direct_message(&ctx.http, |m| m.embed(|e| e
                                .title(format!("Synced {} with user {}", dc_user.tag(), user.osu_username))
    
                                .thumbnail(format!("{}/{}", PROFILE_PIC_URL, user.osu_id))
                                .colour(Colour::from_rgb(228, 132, 250))
                                .url(format!("{}/{}/{}", PROFILE_URL, user.osu_id, mode.url()))
                                .footer(|f| f
                                    .icon_url(dc_user.avatar_url().unwrap_or(dc_user.default_avatar_url()))
                                    .text(dc_user.tag())
                                )
                            
                            )).await.unwrap();
                        }
                        Err(e) => {
                            error!("{}", e);
                        }
                    }
                }
            }

            let state = a.clone().state.unwrap();

            let song = a.clone().details;
            let presence = OsuPresence { account: account.clone(), state: state.clone(), mode, song, time };

            let _ch_prsnc = osu.lock().await.get_cached_presence(presence.account.clone()).await;
            
            osu.lock().await.cache_presence(presence.clone()).await;

            let cached_presence = match _ch_prsnc {
                Some(c) => c,
                None => return,
            };

            if cached_presence.state == "Idle" || state == "AFK" { return; }

            //debug!("Cached Presence: {:?}", cached_presence);

            if state == "Idle" {

                // #auto-recent channel
                let channel =  if let Channel::Guild(c) = ctx.cache.channel(749194271373918249).await.unwrap() { c } else { return };

                let discord_id = *update.presence.user_id.as_u64();

                let user = database.lock().await.get_user(discord_id).await.unwrap().unwrap();

                let cached_recent = match mode {
                    OsuMode::Standard => user.cached_recent_s,
                    OsuMode::Taiko => user.cached_recent_t,
                    OsuMode::CatchTheBeat => user.cached_recent_c,
                    OsuMode::Mania => user.cached_recent_m,
                };

                check_latest(osu.clone(), database.clone(), discord_id, cached_presence, mode, cached_recent, channel, ctx.http.clone());
            }
        }
    }
}

use serenity::http::client::Http;
use tokio::time::delay_for;
use tokio::time::Duration;

const WAIT_TIME: u64 = 10;

fn check_latest(osu: Arc<Mutex<Osu>>, db: Arc<Mutex<Db>>, discord_id: u64, cached_presence: OsuPresence,
    mode: OsuMode, cached_recent: String, channel: GuildChannel, http: Arc<Http>) {

    tokio::spawn(async move {

        let mut tries: u8 = 0;

        while tries <= 3 {
            let recents = osu.lock().await.get_recent(cached_presence.account.clone(), cached_presence.mode.clone(), 1).await
                .expect("Error getting recent score");
            
            let recent = match recents.first() {
                Some(r) => r,
                None => {

                    //debug!("No recent found {:?}", cached_presence);

                    tries += 1;

                    delay_for(Duration::from_secs(WAIT_TIME)).await;

                    continue;
                }
            };

            if !recent.compare(cached_presence.clone()) {
                
                //debug!("Recent doesn't match {:?}\n{:?}", recent, cached_presence);

                tries += 1;

                delay_for(Duration::from_secs(WAIT_TIME)).await;

                continue;
            }

            if recent.hash() == cached_recent {
                //debug!("Recent already cached: {:?}", recent);
                return; 
            }

            if let OsuGrade::Fail = recent.rank { 
                //debug!("Recent is a fail: {:?}", recent);

                return; 
            }
            else {

                // TODO: Configurables
            
                let profile = match osu.lock().await.get_user(cached_presence.account.clone(), cached_presence.mode.clone()).await {
                    Ok(p) => p,
                    Err(_) => return,
                };

                channel.send_message(http, |m| recent.embed(m, profile)).await.expect("Error sending message");

            }

            let builder = match mode {
                OsuMode::Standard => DbUserUpdater::new(discord_id).cached_recent_s(recent.hash()),
                OsuMode::Taiko => DbUserUpdater::new(discord_id).cached_recent_t(recent.hash()),
                OsuMode::CatchTheBeat => DbUserUpdater::new(discord_id).cached_recent_c(recent.hash()),
                OsuMode::Mania => DbUserUpdater::new(discord_id).cached_recent_m(recent.hash()),
            };

            match db.lock().await.update_user(builder).await {
                Err(e) => {
                    error!("Error updating user: {:?}", e);
                }

                _ => ()
            }

            return;
        }

        //debug!("Failed after 3 tries: {:?}", cached_presence);
    });
}
